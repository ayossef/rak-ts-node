import React from "react";
import { useState } from "react";
import Checkout from "./Checkout";
function Cart(props) {
  const productsList = props.products;
  const [itemsCount, setItemsCount] = useState(0)

    const update = ()=>setItemsCount(itemsCount+1)
  return (
    <div>
      Cart
      {productsList.map((item) => {
        return <p>{item}</p>;
      })}
      <button onClick={update}>Add Item to Cart</button>
      <h4>You have # {itemsCount} in your cart</h4>
     
    </div>
  );
}
export default Cart;
