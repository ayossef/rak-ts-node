import React from 'react'
import PropTypes from 'prop-types'

function Checkout(props) {
  return (
    <div>Checkout
        <h5> You should pay {props.amount}</h5>
    </div>
  )
}

Checkout.propTypes = {}

export default Checkout
