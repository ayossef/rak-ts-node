import React from 'react'
import PropTypes from 'prop-types'

function Food(props) {
  return (
    <div> {props.foodList.length!=0 && <h3>{props.meal}</h3>}
    {props.foodList.length==0 && <h3>No {props.meal} available today</h3>}
      <ol>
      {props.foodList.map((item)=>{return <li>{item}</li>})}
      </ol>
    </div>
  )
}

export default Food
