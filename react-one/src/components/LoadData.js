import React from 'react'

function LoadData() {
   const [listOfTodos, setListOfTodos] = useState([])
    fetch('').then((data)=> data.json()).then((jsonData)=> setListOfTodos(jsonData.data))
  return (
    <div>LoadData
        {listOfTodos.map((item)=> {return <div>
            <h4>{item.title}</h4>
            <h5>{item.description}</h5>
        </div>
        })}

    </div>
  )
}

export default LoadData