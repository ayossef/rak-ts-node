import logo from "./logo.svg";
import "./App.css";
import Food from "./components/Food";
import Cart from "./components/Cart";
import Checkout from "./components/Checkout";
function App() {
  const breakfastlist = [
    "Cup Cakes",
    "Mahfouz Coffee",
    "Banana elyoum",
    "fetar el gom3a",
    "Man2oush and karak by Rameesh",
  ];
  const launchList = ["Pizza", "Pasta", "Tandouri Chicken", "Beriany"];
  const dinnerList = [];
  const message = "Why he didn't considered as HTML?";
  const userLoggedIn = true;
  return (
    <div className="App">
      <header className="App-header">
        <Cart products={launchList}></Cart>
        <Checkout amount={100}></Checkout>
      </header>
    </div>
  );
}

export default App;
