import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { selectCount } from './counterSlice';
import styles from './Counter.module.css';
import { resetToZero } from './counterSlice';

function Checkout() {
    const count = useSelector(selectCount);
    const dispatch = useDispatch()
  return (
    <div>Checkout
        You should pay {count*10} AED 

        <button
        className={styles.button}
        aria-label="Increment value"
        onClick={()=>dispatch(resetToZero())}>
            Already Paid
        </button>
    </div>
  )
}

export default Checkout