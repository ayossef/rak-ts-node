import { render, screen } from '@testing-library/react';
import App from './App';

test('renders learn react link', () => {
  render(<App />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});



test('Testing the Add logic',() =>{
  // AAA 
  // Arrange
  const num1 = 10
  const num2 = 20

  // Act
  let result = num1 - num2

  // Assert
  expect(result).toBe(30)

})