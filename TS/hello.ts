class User{
    name: string
    age: number
    email: string
    userNumber: number|string
    constructor(email:string){
        this.email = email
    }
    printUser(){
        console.log('Hello, '+this.name)
    }
    ageCheck(x:number){
        if( x ) {
            return true
        } else {
            return x+'1'
        }
    }
    hasValidEmail(){
        if(this.email.length < 10){
            return false
        }else {
            return true
        }
    }
}


function xyz(){
    let userOne = new User('user@one.com')
    userOne.age = 45
    userOne.name = 'Mr. First User'

    userOne.printUser()
    userOne.hasValidEmail() ? console.log('email is valid'): console.log('not Valid')
}

xyz()