var User = /** @class */ (function () {
    function User(email) {
        this.email = email;
    }
    User.prototype.printUser = function () {
        console.log('Hello, ' + this.name);
    };
    User.prototype.ageCheck = function (x) {
        if (x) {
            return true;
        }
        else {
            return x + '1';
        }
    };
    User.prototype.hasValidEmail = function () {
        if (this.email.length < 10) {
            return false;
        }
        else {
            return true;
        }
    };
    return User;
}());
function xyz() {
    var userOne = new User('user@one.com');
    userOne.age = 45;
    userOne.name = 'Mr. First User';
    userOne.printUser();
    if (userOne.hasValidEmail()) {
        console.log('email is valid');
    }
    else {
        console.log('Please update your email');
    }
}
xyz();
