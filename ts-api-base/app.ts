import express from 'express'
import { itemsRouter } from './src/router/Items'
import { menuRouter } from './src/router/Menu'

const app = express()

app.get('/',
    (req, resp)=>{resp.status(200).send('Express Here')}
)
app.get('/about',()=>{})

app.use('/items', itemsRouter)
app.use('/menu', menuRouter)

app.listen(444, ()=>{console.log('Getting turkish coffee ready .. by Mahfouz')})
console.log('End.')