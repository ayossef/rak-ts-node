import express from 'express'
import { findAll, findById, findByName } from '../controller/Menu'


export const menuRouter = express.Router()


menuRouter.get('/', findAll)
menuRouter.get('/:id', findById)
menuRouter.get('/findByName/:name', findByName)


