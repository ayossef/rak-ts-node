import express , {Router}  from 'express'
import {test, findAll, create, findFirst, cityWithoutItems} from '../controller/Items'
 
export const itemsRouter = express.Router()

itemsRouter.get('/test', test)
itemsRouter.get('/', findAll)
itemsRouter.get('/first',findFirst)
itemsRouter.get('/city', cityWithoutItems)
