export class MenuItem{
    //id: number
    name: string
    price: number
    //type: string
    //imgURL: string

    constructor(name:string, price:number){
        this.name = name
        this.price = price
    }    
}