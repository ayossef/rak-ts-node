import { Request, Response, NextFunction } from "express";
import { MenuItem } from "../model/MenuItem"; 
const menuItems = [new MenuItem('Pizza', 30), new MenuItem('Pasta', 40), new MenuItem('7mam ma7shy',1000000)]

export const findAll = (req:Request, resp:Response, next:NextFunction) => {
    resp.status(200).json({
        data: menuItems,
        code: 0,
        message: "OK"
    })
}
export const findById = (req:Request, resp:Response, next:NextFunction) => {

}

export const findByName = (req:Request, resp:Response, next:NextFunction) => {
    const serachve= req.params.name
    const itemFound= menuItems.filter((item)=>{
        return item.name===serachve
    })
    
    resp.status(200).json({
        data: itemFound,
        code: 0,
        message: "OK"
    })
}

export const create = (req:Request, resp:Response, next:NextFunction) => {
    
}

export const update = (req:Request, resp:Response, next:NextFunction) => {
    
}

export const remove = (req:Request, resp:Response, next:NextFunction) => {
    
}