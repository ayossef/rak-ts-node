import {Request, Response, NextFunction} from 'express'
import axios, { AxiosResponse } from 'axios';


const itemsList = ['Item One', 'Item Two', 'Item Three','item Four']

export const test= (req:Request, resp: Response, next:NextFunction) =>{
    resp.status(200).json({
        message:'Test OK'
    })
}
export const cityWithoutItems = async (req:Request, resp:Response, next:NextFunction)=>{
    const citiesUrl = 'https://chamber-test.com/ords/admin/list/city'
    let result: AxiosResponse = await axios.get(citiesUrl);
    let cities = result.data;

    return resp.status(200).json(cities.items);
}
export const findFirst= (req:Request, resp:Response, next:NextFunction)=>{
    resp.status(200).json({items:{
        data: itemsList[0],
        code: 0,
        message: "OK"
    }})
}

export const findAll = (req:Request, 
    resp:Response, 
    next: NextFunction) =>  {
    resp.status(200).json({
        data: itemsList,
        code: 0,
        message: "OK"
    })
}


export const create = () => {

}