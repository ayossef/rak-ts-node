import pool from '../dbconfig/dbconnector';

class TodosController {

    public async get(req, res) {
        try {
            const client = await pool.connect();

            const sql = "SELECT * FROM todos";
            const { rows } = await client.query(sql);
            const todos = rows;
            if(todos.length === 0) {res.send({message:"Nothing is foubnd", data:todos})}
            client.release();

            res.send({message:"OK",data:todos});
        } catch (error) {
            res.status(400).send(error);
        }
    }
}

export default TodosController;