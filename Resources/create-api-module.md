---
marp: true
---
# Create API Module 


## 1. Create Model
In the *src/model* folder 

---

## 2. Create Router
In the *src/routes* create a router.ts file 
Inside the router file, create 
```js
- get('/', findAll)
- get('/:id', findById)
- post('/', create)
- put('/:id', update)
- delete('/:id', delete)
```
---
## 3. Add Router inside the app

inside the app.ts file 
```js
app.use('/menu', menuRouter)
```
---
## 4. Create controller
Create new file under src/conroller
crete the 5 CRUD Methods:
- findAll
- findById
- create
- update
- delete 

---