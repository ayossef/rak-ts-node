import express from 'express'
import { getAllUsers,getById, hello } from '../controllers/Users'

export const usersRouter = express.Router()

usersRouter.get('/',getAllUsers)
usersRouter.get('/:id', getById)
usersRouter.get('/hello', hello)