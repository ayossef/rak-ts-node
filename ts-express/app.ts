import express from 'express';
import { usersRouter } from './src/routes/Users';

const app = express();

app.get('/', function (req, res) {
  res.send('Hello World');
});
app.use('/users', usersRouter)
app.listen(3000);